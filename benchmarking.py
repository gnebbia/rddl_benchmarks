#!/usr/bin/env python
# coding: utf-8

import random
import csv
import sys
import time
from random import randrange
import numpy
import gym
from pyRDDLGym import RDDLEnv
from pyRDDLGym.Core.Policies.Agents import RandomAgent
from pyRDDLGym.Core.Policies.Agents import BaseAgent
from collections import OrderedDict





# Attackers and defenders look the same
class PassiveCyberAgent(BaseAgent):
    def __init__(self, action_space):
        self.action_space = action_space

    def sample_action(self, state=None):
        selected_action = next(iter(self.action_space.spaces))
        action = {selected_action: self.action_space[selected_action]}
        action[selected_action] = 0
        #print(f'action = {action}')
        return action

class RandomCyberAgent(BaseAgent):
    def __init__(self, action_space, seed=None):
        self.action_space = action_space
        self.rng = random.Random(seed)
        if seed is not None:
            self.action_space.seed(seed)

    def sample_action(self, state=None):
        s = self.action_space.sample()
        action = {}
        selected_action = self.rng.sample(list(s), 1)[0]
        action[selected_action] = s[selected_action]
        return action

class KeyboardCyberAgent(BaseAgent):
    def __init__(self, action_space, seed=None):
        self.action_space = action_space

    def sample_action(self, obs=None, state=None):
        # available_actions = list(self.action_space.spaces.keys())
        obs = {k for k,v in obs.items() if v}

        state = {k.replace("state", "obs") for k,v in state.items() if v}
        obs = obs - state
        available_actions = obs if obs else list(self.action_space.spaces.keys())


        available_actions = {k.replace("obs", "action") for k in available_actions}

        #print("Available actions:")
        action_index = {k:v for k,v in enumerate(available_actions)}
        for i, action in action_index.items():
            pass
            #print(f"{i}. {action}")

        #print("NUMBER OF AVAILABLE ACTIONS")
        num_actions = len(available_actions)  
        #print(num_actions)

        # selected_index = int(input("Enter the index of the action you want to take: "))
        selected_index = randrange(num_actions)
        #print("SELECTED " + str(selected_index))


        if selected_index < 0 or selected_index >= len(available_actions):
            #print("Invalid index. Using a default action.")
            selected_index = 0

        selected_action = action_index[selected_index]

        return {selected_action: 1}

# Wrapper to sample both an attack and a defense action
class DoubleAgent(BaseAgent):

    def __init__(self, action_space, seed=None, attacker_policy='random', defender_policy='passive'):
        attack_steps = gym.spaces.dict.Dict({k: v for k, v in action_space.items() if 'action' in k})
        defense_steps = gym.spaces.dict.Dict({k: v for k, v in action_space.items() if 'defense' in k})
        if attacker_policy == 'passive':
            self.attacker = PassiveCyberAgent(attack_steps)
        elif attacker_policy == 'keyboard':
            self.attacker = KeyboardCyberAgent(attack_steps)
        else:
            self.attacker = RandomCyberAgent(attack_steps, seed=seed)
        if defender_policy == 'passive':
            self.defender = PassiveCyberAgent(defense_steps)
        elif defender_policy == 'keyboard':
            self.defender = KeyboardCyberAgent(defense_steps)
        else:
            self.defender = RandomCyberAgent(defense_steps, seed=seed)

    def sample_action(self, obs=None, state=None):
        attack_action = self.attacker.sample_action(obs=obs, state=state)
        # defense_action = self.defender.sample_action(obs=obs, state=state)
        #print(f'attack_action = {attack_action}')
        # print(f'defense_action = {defense_action}')
        # action = attack_action | defense_action
        action = attack_action
        return action



DOMAIN_FILE='corelang.rddl'
INSTANCE_FILES=sys.argv[1:]
print(INSTANCE_FILES)

for ifile in INSTANCE_FILES:
    for _ in range(1):
        myEnv = RDDLEnv.RDDLEnv(domain=DOMAIN_FILE, instance=ifile)
        #print(f'myEnv.action_space = {myEnv.action_space}')
        #agent = DoubleAgent(action_space=myEnv.action_space, attacker_policy='keyboard', defender_policy='passive', seed=42)
        agent = DoubleAgent(action_space=myEnv.action_space, attacker_policy='keyboard', defender_policy='keyboard', seed=42)

        myEnv.observation_space


        total_reward = 0

        loading_start_time = time.time()
        obs = myEnv.reset()
        loading_end_time = time.time()

        print(f'Loading time {loading_end_time-loading_start_time} s.')
        loading_time=loading_end_time-loading_start_time

        #print(obs)

        print(f'step         = 0')
        print(f'Entry points = {[attackstep for attackstep, value in obs.items() if type(value) is numpy.bool_ and value == True]}')


        # Print Actions Performed
        state = myEnv.state
        state

        # Print Possible Actions/What I can Observe
        obs.items()

        initial_attack_step = 'action_Identity_successfulAssume___id1'
        # action = agent.sample_action()
        # next_obs, reward, done, info = myEnv.step(action)




        myEnv.horizon = 3

        start_time = time.time()


        for step in range(myEnv.horizon):
            print(f"STEP {step}")
            #print(f'Step id = {step}')
            #print(f'Compromised nodes = {[attackstep for attackstep, value in state.items() if type(value) is numpy.bool_ and value == True]}')
            if step == 0:
                action = {initial_attack_step: True}
            else: 
                action = agent.sample_action(obs, state)
            #print(f'Observations = {[attackstep for attackstep, value in obs.items() if type(value) is numpy.bool_ and value == True]}')
            next_obs, reward, done, info = myEnv.step(action)
            state = myEnv.state
            total_reward += reward
            #print()
            #print(f'Action = {action}')
            #print(f'Observations = {[attackstep for attackstep, value in next_obs.items() if type(value) is numpy.bool_ and value == True]}')
            #print(f'reward = {reward}')
            obs = next_obs
            if done:
                break

        end_time = time.time()

        stepping_time=end_time-start_time
        print()
        print(f'episode ended with reward {total_reward}. Execution time was {end_time-start_time} s.')
        print(f'Total number of steps {myEnv.horizon}')

        myEnv.close()

        avg_step = stepping_time / myEnv.horizon

        with open('benchmarking_3.csv', 'a+') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',')
            spamwriter.writerow([ifile,loading_time,stepping_time,avg_step])
